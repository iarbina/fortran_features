program ieee
  use, intrinsic :: ieee_arithmetic
  use, intrinsic :: ieee_features
  use, intrinsic :: ieee_exceptions
  use, intrinsic :: iso_fortran_env, sp=>real32  &
                                   , dp=>real64
  implicit none
  real(dp) :: x
  real(sp) :: x_sp
  logical :: ieee_flag_value
  type(ieee_flag_type) :: ieee_flag
  type(ieee_status_type) :: ieee_status_values
  type(ieee_class_type) :: ieee_class
  print'(" > Huge: ", g0)', huge(x)
  print'(" > Huge: ", g0)', -huge(x)
  print'(" > Tiny: ", g0)', tiny(x)
  print'(" > Tiny: ", g0)', -tiny(x)
  print'(" > Is NaN?: ", g0)', ieee_is_nan(x)
  print'(" > : ", 2g0)', 0.0_dp, -0.0_dp 
  if (0.0_dp > -0.0_dp) print'("True")'
  if (0.0_dp < -0.0_dp) print'("True")'
  print'(" > nan support?: ", g0)', ieee_support_nan(x_sp)
  print'(" > nan support?: ", g0)', ieee_support_nan(x)
  x = -5.d0 
  x = sqrt(x)
  print'(" > sqrt(-): ", g0)', x
  call ieee_get_flag (ieee_flag, ieee_flag_value)
  print'(" > IEEE get flag value: ", g0)', ieee_flag_value
  call ieee_set_flag (ieee_flag, .true.)
  print'(" > IEEE set flag value: ", g0)', ieee_flag_value
  call ieee_get_flag (ieee_flag, ieee_flag_value)
  print'(" > IEEE get flag value: ", g0)', ieee_flag_value
  print'(" > IEEE support flag: ", g0)', ieee_support_flag (ieee_invalid) 
  call ieee_get_status (ieee_status_values)
end program ieee
