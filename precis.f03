program precis
  use, intrinsic :: iso_fortran_env, sp=>real32,  &
                                     dp=>real64
  implicit none
  integer(int32) :: int_32
  integer(int64) :: int_64
  real(sp) :: re_sp
  real(dp) :: re_dp
  print'(":: Numeric inquiry functions")', 
  print'(" > digits int_32     = ",g0)', digits(int_32)
  print'(" > digits int_64     = ",g0)', digits(int_64)
  print'(" > digits int_32     = ",g0)', digits(int_32)
  print'(" > digits int_64     = ",g0)', digits(int_64)
  print'(" > digits re_sp      = ",g0)', digits(re_sp)
  print'(" > digits re_dp      = ",g0)', digits(re_dp)
  print'(" > epsilon re_sp     = ",g0)', epsilon(re_sp)
  print'(" > epsilon re_dp     = ",g0)', epsilon(re_dp)
  print'(" > precision re_sp   = ",g0)', precision(re_sp)
  print'(" > precision re_dp   = ",g0)', precision(re_dp)
  print'(" > huge int_32       = ",g0)', huge(int_32)
  print'(" > huge int_64       = ",g0)', huge(int_64)
  print'(" > huge re_sp        = ",g0)', huge(re_sp)
  print'(" > huge re_dp        = ",g0)', huge(re_dp)
  print'(" > tiny re_sp        = ",g0)', tiny(re_sp)
  print'(" > tiny re_dp        = ",g0)', tiny(re_dp)
  print'(" > maxexponent re_sp = ",g0)', maxexponent(re_sp)
  print'(" > maxexponent re_dp = ",g0)', maxexponent(re_dp)
  print'(" > minexponent re_sp = ",g0)', minexponent(re_sp)
  print'(" > minexponent re_dp = ",g0)', minexponent(re_dp)
  print'(" > radix re_sp       = ",g0)', radix(re_sp)
  print'(" > radix re_dp       = ",g0)', radix(re_dp)
  print'(" > range re_sp       = ",g0)', range(re_sp)
  print'(" > range re_dp       = ",g0)', range(re_dp)
end program precis
