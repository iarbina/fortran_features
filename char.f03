program pchar
  implicit none
  integer :: i
  character(len=:), allocatable :: mychar
  character(len=*), parameter :: alphabet = &
    'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  character(len=*), parameter :: numbers = '0123456789'
  i = 76
  mychar = 'Hallo Weld!'
  print'(" >> achar : ",g0)', achar(i)
  print'(" >> char  : ",g0)', char(i)
  print'(" >> iachar: ",g0)', iachar(mychar)
  print'(" >> ichar : ",g0)', ichar(mychar)
  do i=1, 200
     print'(" >> char(",g0,") = ",g0)', i, char(i)
  end do
  do i=1, 200
     print'(" >> achar(",g0,") = ",g0)', i, achar(i)
  end do
  do i=1, len(numbers)
     print'(" >> iachar(",g0,") = ",g0)', &
       numbers(i:i), iachar(numbers(i:i))
  end do
  print'(" >> ",g0)', achar(iachar('1'))
  print'(" >> ",g0)', iachar(achar(49))
end program pchar
