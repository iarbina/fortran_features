program bits
  implicit none
  integer :: i, n
  n = 2
  print'(":: log10(",g0,") = ",g0)', n, ceiling(log10(1.0*n))
  print'(":: Bit study of numer ",g0," of bit size ",g0)', n, bit_size(n)
  do i=1, bit_size(n)
     print'("   |-> btest(",g0,",",g0,") = ",g0)', n, i, btest(n, i)
  end do
end program bits
